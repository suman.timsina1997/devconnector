const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
const Validator = require("validator");

const Profile = require("../../models/Profile");
const User = require("../../models/User");

const validateProfileInput = require("../../validation/profile");
const validateExperienceInput = require("../../validation/experience");
const validateEducationInput = require("../../validation/education");



router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const errors = {};
    Profile.findOne({ user: req.user.id })
      .populate("user", ["name", "avatar"])
      .then((profile) => {
        if (!profile) {
          errors.noprofile = "No Profile has been found for this user.";
          return res.status(404).json(errors);
        }
        res.json(profile);
      })
      .catch((err) => {
        console.log(res.status(404).json(err));
      });
  }
);

//@desc Get all user profile and sort

router.get("/all", (req, res) => {
  const errors = {};
  Profile.find()
    .populate("user", ["name", "avatar"])
    .then((profiles) => {
      if (!profiles) {
        errors.noprofile = "There are no  profiles for users!";
        return res.status(404).json(errors);
      }
      return res.json(profiles);
    })
    .catch((err) => {
      return res.status(404).json(err);
    });
});

//@desc Get profile by handle

router.get("/handle/:handle", (req, res) => {
  const errors = {};
  let handle = req.params.handle;
  Profile.findOne({ handle: handle })
    .populate("user", ["name", "avatar"])
    .then((profile) => {
      if (!profile) {
        errors.noprofile = "There is no  profile found for this handle!";
        return res.status(404).json(errors);
      }
      return res.json(profile);
    })
    .catch((err) => {
      return res.status(404).json(err);
    });
});

//@desc Get profile by user_id

router.get("/user/:user_id", (req, res) => {
  const errors = {};
  Profile.findOne({ user: req.params.user_id })
    .populate("user", ["name", "avatar"])
    .then((profile) => {
      if (!profile) {
        errors.noprofile = "There is no  profile found for this user ID!";
        return res.status(404).json(errors);
      }
      return res.json(profile);
    })
    .catch((err) => {
      errors.noprofile = "There is no  profile found for this user ID!";
      return res.status(404).json(errors);
    });
});

//@desc create and update user profile input

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateProfileInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const profileFields = {};
    profileFields.user = req.user.id;
    if (req.body.handle) profileFields.handle = req.body.handle;
    if (req.body.company) profileFields.company = req.body.company;
    if (req.body.website) profileFields.website = req.body.website;
    if (req.body.location) profileFields.location = req.body.location;
    if (req.body.bio) profileFields.bio = req.body.bio;
    if (req.body.status) profileFields.status = req.body.status;
    if (req.body.gitusername) profileFields.gitusername = req.body.gitusername;

    if (typeof req.body.skills !== "undefined") {
      profileFields.skills = req.body.skills.split(",");
    }
    profileFields.social = {};

    if (req.body.youtube) profileFields.social.youtube = req.body.youtube;
    if (req.body.twitter) profileFields.social.twitter = req.body.twitter;
    if (req.body.facebook) profileFields.social.facebook = req.body.facebook;
    if (req.body.instagram) profileFields.social.instagram = req.body.instagram;
    if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;

    Profile.findOne({ user: req.user.id })
      .then((profile) => {
        if (profile) {
          //update
          Profile.findOneAndUpdate(
            { user: req.user.id },
            { $set: profileFields },
            { new: true }
          )
            .then((profile) => {
              res.json(profile);
            })
            .catch((err) => {
              res.status(404).json(err);
            });
        } else {
          //create
          Profile.findOne({ handle: profileFields.handle })
            .then((profile) => {
              if (profile) {
                errors.handle = "That handle already exists";
                return res.status(400).json(errors);
              }
              new Profile(profileFields).save().then((profile) => {
                res.json(profile);
              });
            })
            .catch();
        }
      })
      .catch();
  }
);

//@desc Add exxperience to profile

router.post(
  "/experience",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateExperienceInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    Profile.findOne({ user: req.user.id })
      .then((profile) => {
        if (!profile) {
          errors.profile = "There is no profile for this user.";
          return res.status(404).json(errors);
        }
        const newExp = {
          title: req.body.title,
          company: req.body.company,
          location: req.body.location,
          from: req.body.from,
          to: req.body.to,
          current: req.body.current,
          description: req.body.description,
        };
        profile.experience.unshift(newExp);
        profile
          .save()
          .then((profile) => {
            return res.json(profile);
          })
          .catch((err) => {
            return res.status(404).json(err);
          });
      })
      .catch();
  }
);

//@desc Add education to profile

router.post(
  "/education",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateEducationInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    Profile.findOne({ user: req.user.id })
      .then((profile) => {
        if (!profile) {
          errors.profile = "There is no profile for this user.";
          return res.status(404).json(errors);
        }
        const newEdu = {
          school: req.body.school,
          degree: req.body.degree,
          fieldofstudy: req.body.fieldofstudy,
          from: req.body.from,
          to: req.body.to,
          current: req.body.current,
          description: req.body.description,
        };
        profile.education.unshift(newEdu);
        profile
          .save()
          .then((profile) => {
            return res.json(profile);
          })
          .catch((err) => {
            return res.status(404).json(err);
          });
      })
      .catch();
  }
);

//@desc delete experience from profile

router.delete(
  "/experience/:exp_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then((profile) => {
        // Get remove index
        const removeIndex = profile.experience
          .map((item) => item.id)
          .indexOf(req.params.exp_id);

        // Splice out of array
        profile.experience.splice(removeIndex, 1);

        // Save
        profile.save().then((profile) => res.json(profile));
      })
      .catch((err) => res.status(404).json(err));
  }
);

router.delete(
  "/education/:edu_id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then((profile) => {
        // Get remove index
        const removeIndex = profile.education
          .map((item) => item.id)
          .indexOf(req.params.edu_id);

        // Splice out of array
        profile.education.splice(removeIndex, 1);

        // Save
        profile.save().then((profile) => res.json(profile));
      })
      .catch((err) => res.status(404).json(err));
  }
);

router.delete(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOneAndRemove({ user: req.user.id }).then(() => {
      User.findOneAndRemove({ _id: req.user.id }).then(() =>
        res.json({ success: true })
      );
    });
  }
);

module.exports = router;
