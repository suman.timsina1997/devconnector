const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const User = require("../../models/User");

//Load input validator

const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

const keys = require("../../config/keys").secretOrKey;

router.get("/test", (req, res) => {
  res.json({ Message: "Users Route" });
});

//user regostration
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = 'Email already exists';
      return res.status(400).json(errors);
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm' // Default
      });

      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        avatar,
        password: req.body.password
      });

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
    }
  });
});

router.post("/login", (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: email })
    .then((user) => {
      if (!user) {
        errors.email = "User can not be found with this email!";
        return res
          .status(404)
          .json(errors);
      }
      bcrypt
        .compare(password, user.password)
        .then((isMatched) => {
          if (isMatched) {
            const payload = {
              id: user.id,
              name: user.name,
              avatar: user.avatar,
            }; //jwt payload
            //Sign Token
            jwt.sign(payload, keys, { expiresIn: 36000 }, (err, token) => {
              res.json({ success: true, token: "Bearer " + token });
            });
          } else {
            errors.password = "Password incorrect"
            return res.status(400).json(errors);
          }
        })
        .catch();
    })
    .catch((err) => {
      console.log(err);
    });
});

router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
    });
  }
);

module.exports = router;
