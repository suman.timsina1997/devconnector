const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
const Post = require("../../models/Post");
const Profile = require("../../models/Profile");

const validatePostInput = require("../../validation/post");

router.get("/test", (req, res) => {
  res.json({ Message: "Posts Route" });
});

//@desc fetch all posts
router.get("/", (req, res) => {
  Post.find()
    .sort({ date: -1 })
    .then((posts) => {
      if (!posts) {
        return req.status(404).json({ posts: "No post been found" });
      }
      res.json(posts);
    })
    .catch((err) => {
      res.json(err);
    });
});

//@desc fetch  post by id
router.get("/:post_id", (req, res) => {
  Post.findById(req.params.post_id)
    .then((post) => {
      if (!post) {
        return res
          .status(404)
          .json({ post: "No post been found for this ID!" });
      }
      res.json(post);
    })
    .catch((err) => {
      res.json(err);
    });
});

//@desc post new POSTS
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const newPost = new Post({
      text: req.body.text,
      name: req.body.name,
      avatar: req.body.avatar,
      user: req.user.id,
    });

    newPost
      .save()
      .then((post) => {
        res.json(post);
      })
      .catch((err) => {
        res.status(404).json(err);
      });
  }
);

//@desc delete post by ID

router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ id: req.user.id })
      .then((profile) => {
        Post.findById(req.params.id).then((post) => {
          if (post.user.toString() !== req.user.id) {
            return res
              .status(401)
              .json({ notauthorized: " user not authorized" });
          }
          post
            .remove()
            .then(() => {
              res.json({ success: true });
            })
            .catch((err) => {
              res.status(404).json({ postnotfound: "No pst found!" });
            });
        });
      })
      .catch((err) => {
        res.status(404).json({ nopostfound: "Post not found" });
      });
  }
);

//@desc like post

router.post(
  "/like/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ id: req.user.id })
      .then((profile) => {
        Post.findById(req.params.id).then((post) => {
          if (
            post.likes.filter((like) => like.user.toString() === req.user.id)
              .length > 0
          ) {
            return res
              .status(400)
              .json({ alreadyliked: "User already liked this post" });
          }
          post.likes.unshift({ user: req.user.id });
          post.save().then((post) => {
            res.json(post);
          });
        });
      })
      .catch((err) => {
        res.status(404).json({ nopostfound: "Post not found" });
      });
  }
);

//@desc unlike post

router.post(
  "/unlike/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ id: req.user.id })
      .then((profile) => {
        Post.findById(req.params.id).then((post) => {
          if (
            post.likes.filter((like) => like.user.toString() === req.user.id)
              .length === 0
          ) {
            return res
              .status(400)
              .json({ notliked: "You have not yet liked this post" });
          }
          const removeIndex = post.likes
            .map((item) => item.user.toString())
            .indexOf(req.user.id);

          post.likes.splice(removeIndex, 1);
          post.save().then((post) => {
            res.json(post);
          });
        });
      })
      .catch((err) => {
        res.status(404).json({ nopostfound: "Post not found" });
      });
  }
);

//@desc post comment on posts

router.post(
  "/comment/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    Post.findById(req.params.id).then((post) => {
      const newComment = {
        text: req.body.text,
        name: req.body.name,
        avatar: req.body.avatar,
        user: req.user.id,
      };
      post.comments.unshift(newComment);

      post
        .save()
        .then((post) => {
          res.json(post);
        })
        .catch((err) => {
          res.status(404).json({ nopostfound: "No post found" });
        });
    });
  }
);

//@desc remove comment from posts

router.delete(
  '/comment/:id/:comment_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Post.findById(req.params.id)
      .then(post => {
        // Check to see if comment exists
        if (
          post.comments.filter(
            comment => comment._id.toString() === req.params.comment_id
          ).length === 0
        ) {
          return res
            .status(404)
            .json({ commentnotexists: 'Comment does not exist' });
        }

        // Get remove index
        const removeIndex = post.comments
          .map(item => item._id.toString())
          .indexOf(req.params.comment_id);

        // Splice comment out of array
        post.comments.splice(removeIndex, 1);

        post.save().then(post => res.json(post));
      })
      .catch(err => res.status(404).json({ postnotfound: 'No post found' }));
  }
);

module.exports = router;
